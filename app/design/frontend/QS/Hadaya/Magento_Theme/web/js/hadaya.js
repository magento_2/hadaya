;(function($){
    $(document).ready(function($){

        /**
         * Select gift banner
         */

        var whos_gift = $('.gift-select-for');
        var search_btn = $('.find-best-gifts');
        var holiday = $('.gift-select-what');
        var response_block = $('.colapse-menu-select');
        var holiday_response_item_ev = $('.colapse-menu-select .everyday-occasion li');
        var holiday_response_item_pb = $('.colapse-menu-select .public-occasion li');
        var open_menu = false;

        whos_gift.on('click', function(){
            // test pop up from holiday example
        });

        search_btn.on('click', function(){
            alert('Ajax request for search');
        });

        holiday.on('click', function(){
            SelectBlockItem(this);
        });

        holiday_response_item_ev.on('click', function(){
            if($(this).text().trim().length > 12){
                holiday.html($(this).text().trim().substr(0, 10) + '... <' + $(holiday).html().split('<')[1]);
            }
            else{
                holiday.html($(this).text().trim() + '<' + $(holiday).html().split('<')[1]);
            }
        });

        holiday_response_item_pb.on('click', function(){
            if($(this).text().trim().length > 12){
                holiday.html($(this).text().trim().substr(0, 10) + '... <' + $(holiday).html().split('<')[1]);
            }
            else{
                holiday.html($(this).text().trim() + '<' + $(holiday).html().split('<')[1]);
            }

        });

        function SelectBlockItem(el){
            if(!open_menu){
                $(el).addClass('selected_menu');
                $(el).parents().find('.left-menu').addClass('yellow-background');
                $(el).find('img').attr('src', $(el).find('img').attr('src').split('small-arrow-down.png')[0]+'white-arrow-down.png');
                // ... some ajax request
                // example of responce
                // responce = '<div class="everyday-occasion"> <h3> Everyday Occasion            </h3>            <ul>            <li class="select-item">            <span>           <img src="http://magento.quartsoft.com/yaroslav/hadaya/pub/static/frontend/QS/Hadaya/en_US/images/menu-icons/anniversary.png">                </span>                Anniversary                </li>                <li>                <span>                <img src="http://magento.quartsoft.com/yaroslav/hadaya/pub/static/frontend/QS/Hadaya/en_US/images/menu-icons/baby.png">                </span>                Baby                </li>                <li>                <span>                <img src="http://magento.quartsoft.com/yaroslav/hadaya/pub/static/frontend/QS/Hadaya/en_US/images/menu-icons/birthday.png">                </span>                Birthday                </li>                <li>                <span>                <img src="http://magento.quartsoft.com/yaroslav/hadaya/pub/static/frontend/QS/Hadaya/en_US/images/menu-icons/congratulations.png">                </span>                Congratulations                </li>                <li>                <span>                <img src="http://magento.quartsoft.com/yaroslav/hadaya/pub/static/frontend/QS/Hadaya/en_US/images/menu-icons/get-well.png">                </span>                Get Well            </li>            <li>            <span>            <img src="http://magento.quartsoft.com/yaroslav/hadaya/pub/static/frontend/QS/Hadaya/en_US/images/menu-icons/housewarming.png">                </span>                Hausewarming                </li>                <li>                <span>                <img src="http://magento.quartsoft.com/yaroslav/hadaya/pub/static/frontend/QS/Hadaya/en_US/images/menu-icons/im-sorry.png">                </span>                I\'m Sorry            </li>            <li>            <span>            <img src="http://magento.quartsoft.com/yaroslav/hadaya/pub/static/frontend/QS/Hadaya/en_US/images/menu-icons/just-because.png">                </span>                Just Because            </li>            <li>            <span>            <img src="http://magento.quartsoft.com/yaroslav/hadaya/pub/static/frontend/QS/Hadaya/en_US/images/menu-icons/love.png">                </span>                Love&amp;Romance                </li>                <li>                <span>                <img src="http://magento.quartsoft.com/yaroslav/hadaya/pub/static/frontend/QS/Hadaya/en_US/images/menu-icons/retriment.png">                </span>                Retirement                </li>                <li>                Sympathy&amp;Funerals                </li>                <li>                Thank You </li>            <li>            The Wedding Collection            </li>            </ul>            </div>   <div class="public-occasion">                <h3>                Public Occasion            </h3>            <ul>            <li>            Ramadan            </li>            <li>            Eid ul-adha            </li>            <li>            New Year            </li>            <li>            Independence day            </li>            </ul>            </div>            </div>';
                // response_block.html(responce);
                response_block.show();
                open_menu = !open_menu;
            }
            else{
                $(el).removeClass('selected_menu');
                $(el).parents().find('.left-menu').removeClass('yellow-background');
                $(el).find('img').attr('src', $(el).find('img').attr('src').split('white-arrow-down.png')[0]+'small-arrow-down.png');
                response_block.hide();
                open_menu = !open_menu;
            }
        }

        /**
         * Right siderbar toogle
         */

        $('.login-button').on('click', function(){
            $('.account-sub-menu').toggle();
            $('.logged-user').toggleClass('logged-user-close');
        });

        $('.view-all-celebrations').on('click', function(){
            if($(this).text().trim() == 'View all friends celebrations'){
                $(this).text('Hide friends celebrations');
                $(this).parent().find('.other-celebrations').css({"display": "inline-block"});
            }
            else{
                $(this).text('View all friends celebrations');
                $(this).parent().find('.other-celebrations').hide();
            }
        });
        $('.see-all-to-poke').on('click', function(){
            if($(this).text().trim() == 'See all friens to poke'){
                $(this).text('Hide some friends to poke');
                $(this).parent().find('.other-poke').css({"display": "inline-block"});
            }
            else{
                $(this).text('See all friens to poke');
                $(this).parent().find('.other-poke').hide();
            }
        });

        /**
         *Checkout on gift page
         */

        



    });
})(jQuery);