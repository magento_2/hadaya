/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        "*": {
            "jquery.slides.min":  "Magento_Theme/js/jquery.slides.min",
            "hadaya":             "Magento_Theme/js/hadaya"
        }
    }
};